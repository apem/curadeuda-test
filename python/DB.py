import mysql.connector

class UseDatabase():

    def __init__( self, config: dict ) -> None:
        """Configura los valores necesarios para la conexión, a partir de un diccionario
        que contiene los parametros para realizar la conexión"""
        self.configuration = config
    
    def __enter__(self) -> 'cursor':
        """Establece la conexión a la base, y devuelve un cursor para ejecutar
        sentencias SQL por medio del mismo"""
        self.conn = mysql.connector.connect(**self.configuration)
        self.cursor = self.conn.cursor()
        return self.cursor
    
    def __exit__(self, exc_type, exc_value, exc_trace) -> None:
        """Realiza las tareas de cierre de cursor y conexión a la base de datos"""
        self.conn.commit()
        self.cursor.close()
        self.conn.close() 
