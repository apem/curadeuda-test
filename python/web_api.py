#!/usr/bin/python3

# Uso de clase personal para conexión y consulta de
# la base de datos
from DB import UseDatabase

from flask import Flask, Response
from collections import OrderedDict
from json import dumps
from re import match

app = Flask(__name__)

# Diccionario con parametros de configuración para la conexión
# a la base de datos 'db_sepomex'
app.config['config'] = {
    'host':'127.0.0.1',
    'user':'curauser',
    'password':'cura123',
    'database':'db_sepomex',
    }

@app.route('/estados')
@app.route('/estados/<string:nombre>/')
def fetch_states(nombre = ''):
    # Almacenará los estados en el orden que los agregue
    tbl_estados = OrderedDict()
    if len(nombre) == 0:
        with UseDatabase(app.config['config']) as cursor:
            _SQL = """SELECT 
                        id,
                        clave_estado,
                        nombre_estado
                    FROM tbl_estados
                    ORDER BY 1;"""
            cursor.execute(_SQL)
            rows = cursor.fetchall()
            for row in rows:
                tbl_estados[row[0]] = {
                    'clave':row[1],
                    'nombre':row[2]
                }
            resp = Response()
            resp.content_type = 'application/json'
            return dumps(tbl_estados)
    else:
        with UseDatabase(app.config['config']) as cursor:
            _SQL = """SELECT 
                        id,
                        clave_estado,
                        nombre_estado
                    FROM tbl_estados
                    WHERE nombre_estado = '{}'
                    ORDER BY 1;""".format(nombre)
            print(_SQL)
            cursor.execute(_SQL)
            rows = cursor.fetchall()
            for row in rows:
                tbl_estados[row[0]] = {
                    'clave':row[1],
                    'nombre':row[2]
                }
            resp = Response()
            resp.content_type = 'application/json'
            return dumps(tbl_estados)



@app.route('/municipios')
@app.route('/municipios/<string:nombre>')
def fetch_municipalities(nombre=''):
    # Almacenara los municipios
    tbl_municipios = OrderedDict()
    if len(nombre) == 0:
        with UseDatabase(app.config['config']) as cursor:
            _SQL = """SELECT 
                        m.id,
                        m.clave_municipio,
                        m.nombre,
                        m.clave_ciudad,
                        m.nombre_ciudad,
                        e.nombre_estado
                    FROM tbl_municipio m
                    JOIN tbl_estados e
                    ON m.clave_estado = e.clave_estado
                    ORDER BY 1;"""
            cursor.execute(_SQL)
            rows = cursor.fetchall()
            for row in rows:
                tbl_municipios[row[0]] = {
                    'clv_mnpio':row[1],
                    'nombre':row[2],
                    'clv_ciudad':row[3],
                    'nom_ciudad':row[4],
                    'estado':row[5],
                }
            return dumps(tbl_municipios)
    else:
        with UseDatabase(app.config['config']) as cursor:
            _SQL = """SELECT 
                        m.id,
                        m.clave_municipio,
                        m.nombre,
                        m.clave_ciudad,
                        m.nombre_ciudad,
                        e.nombre_estado
                    FROM tbl_municipio m
                    JOIN tbl_estados e
                    ON m.clave_estado = e.clave_estado
                    WHERE nombre = '{}'
                    GROUP BY 2
                    ORDER BY 1;""".format(nombre)
            cursor.execute(_SQL)
            rows = cursor.fetchall()
            for row in rows:
                tbl_municipios[row[0]] = {
                    'clv_mnpio':row[1],
                    'nombre':row[2],
                    'clv_ciudad':row[3],
                    'nom_ciudad':row[4],
                    'estado': row[5]
                }
            return dumps(tbl_municipios)

@app.route('/colonias')
@app.route('/colonias/<string:attr>')
def fetch_suburbs(attr=''):
    # Almacenará las colonias
    tbl_colonias = OrderedDict()
    is_postal_code = match('^[0-9]{5}$', attr) # Verificar si es un código postal
    
    if is_postal_code:
        with UseDatabase(app.config['config']) as cursor:
            _SQL = """SELECT 
                        c. id,
                        c.codigo_postal,
                        c.nombre,
                        tc.tipo_colonia,
                        m.nombre_ciudad,
                        m.nombre,
                        e.nombre_estado,
                        m.clave_ciudad,
                        e.clave_estado,
                        c.tipo_zona,
                        c.codigo_oficina
                    FROM tbl_colonia c
                    JOIN cat_tipo_colonia tc ON c.clv_tipo_col = tc.clave
                    JOIN tbl_municipio m ON c.codigo_oficina = m.clave_oficina
                    JOIN tbl_estados e ON e.clave_estado = m.clave_estado
                    WHERE c.codigo_postal = '{}'
                    GROUP BY c.codigo_postal
                    ORDER BY 1;""".format(attr)
            cursor.execute(_SQL)
            rows = cursor.fetchall()
            for row in rows:
                tbl_colonias[row[0]] = {
                    'cp':row[1],         # codigo postal
                    'nombre':row[2],     # nombre colonia
                    'tipo':row[3],       # tipo de colonia
                    'ciudad':row[4],     # nombre de la ciudad
                    'municipio':row[5],  # nombre del municipio
                    'estado':row[6],     # nombre del estado
                    'c_ciudad':row[7],   # clave de la ciudad
                    'c_estado':row[8],   # clave del estado
                    'suelo':row[9],      # tipo de suelo
                    'c_oficina':row[10], # clave de oficina postal
                }
            return dumps(tbl_colonias)

    if len(attr) == 0:
        with UseDatabase(app.config['config']) as cursor:
            _SQL = """SELECT 
                        c. id,
                        c.codigo_postal,
                        c.nombre,
                        tc.tipo_colonia,
                        m.nombre_ciudad,
                        m.nombre,
                        e.nombre_estado,
                        m.clave_ciudad,
                        e.clave_estado,
                        c.tipo_zona,
                        c.codigo_oficina
                    FROM tbl_colonia c
                    JOIN cat_tipo_colonia tc ON c.clv_tipo_col = tc.clave
                    JOIN tbl_municipio m ON c.codigo_oficina = m.clave_oficina
                    JOIN tbl_estados e ON e.clave_estado = m.clave_estado
                    ORDER BY 1;"""
            cursor.execute(_SQL)
            rows = cursor.fetchall()
            for row in rows:
                tbl_colonias[row[0]] = {
                     'cp':row[1],         # codigo postal
                    'nombre':row[2],     # nombre colonia
                    'tipo':row[3],       # tipo de colonia
                    'ciudad':row[4],     # nombre de la ciudad
                    'municipio':row[5],  # nombre del municipio
                    'estado':row[6],     # nombre del estado
                    'c_ciudad':row[7],   # clave de la ciudad
                    'c_estado':row[8],   # clave del estado
                    'suelo':row[9],      # tipo de suelo
                    'c_oficina':row[10], # clave de oficina postal
                }
            return dumps(tbl_colonias)
    else:
        with UseDatabase(app.config['config']) as cursor:
            _SQL = """SELECT 
                        c. id,
                        c.codigo_postal,
                        c.nombre,
                        tc.tipo_colonia,
                        m.nombre_ciudad,
                        m.nombre,
                        e.nombre_estado,
                        m.clave_ciudad,
                        e.clave_estado,
                        c.tipo_zona,
                        c.codigo_oficina
                    FROM tbl_colonia c
                    JOIN cat_tipo_colonia tc ON c.clv_tipo_col = tc.clave
                    JOIN tbl_municipio m ON c.codigo_oficina = m.clave_oficina
                    JOIN tbl_estados e ON e.clave_estado = m.clave_estado
                    WHERE c.nombre = '{}'
                    GROUP BY c.codigo_postal
                    ORDER BY 1;""".format(attr)
            cursor.execute(_SQL)
            rows = cursor.fetchall()
            for row in rows:
                tbl_colonias[row[0]] = {
                     'cp':row[1],         # codigo postal
                    'nombre':row[2],     # nombre colonia
                    'tipo':row[3],       # tipo de colonia
                    'ciudad':row[4],     # nombre de la ciudad
                    'municipio':row[5],  # nombre del municipio
                    'estado':row[6],     # nombre del estado
                    'c_ciudad':row[7],   # clave de la ciudad
                    'c_estado':row[8],   # clave del estado
                    'suelo':row[9],      # tipo de suelo
                    'c_oficina':row[10], # clave de oficina postal
                }
            return dumps(tbl_colonias)



if __name__ == "__main__":
    app.run(debug = True, port = 8000)