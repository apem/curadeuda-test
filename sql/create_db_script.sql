DROP DATABASE IF EXISTS db_sepomex;

CREATE DATABASE IF NOT EXISTS db_sepomex;

USE db_sepomex;

-- CREACION DE TABLA TEMPORAL DONDE SE ALMACENERAN LOS
-- DATOS A PARTIR DEL ARCHIVO DESCARGADO
CREATE TEMPORARY TABLE tmp_tbl_sepomex_data (
    tmp_cp                   CHAR(5) NOT NULL,     -- Código postal de la colonia
    tmp_nom_col              VARCHAR(65) NOT NULL, -- Nombre de la colonia
    tmp_t_col                VARCHAR(25) NOT NULL, -- Tipo de colonia
    tmp_nom_mnpio            VARCHAR(50) NOT NULL, -- Nombre del municipio al que pertenece la colonia
    tmp_nom_estado           VARCHAR(35) NOT NULL, -- Nombre del estado de la republica
    tmp_nom_ciudad           VARCHAR(50) NULL,     -- Nombre de la Ciudad, puede estar en blanco
    tmp_cod_oficina          CHAR(5) NOT NULL,     -- Código de la oficina postal
    tmp_clv_estado           CHAR(2) NOT NULL,     -- Clave numerica del Estado
    tmp_clv_oficina          CHAR(5) NOT NULL,     -- Clave de la oficina postal
    tmp_clv_cp               CHAR(1) NULL,         -- Campo vacio (Asi lo especifica SEPOMEX)
    tmp_clv_tcol             CHAR(2) NOT NULL,     -- Clave del tipo de colonia
    tmp_clv_mnpio            CHAR(2) NOT NULL,     -- Clave numérica del municipio
    tmp_clv_colmnpio         CHAR(4) NOT NULL,     -- Clave numerica de la colonia a nivel municipio
    tmp_tzona                VARCHAR(10) NOT NULL, -- Tipo de zona habitacional de la colonia
    tmp_clv_ciudad           CHAR(2) NULL          -- Clave de la ciudad, puede estar en blanco
);

-- CARGA DE DATOS A PARTIR DEL ARCHIVO, SOLO SE RENOMBRA EL ARCHIVO PARA
-- ESTA PRUEBA, LA ESTRUCTURA DE LOS DATOS SE MANTIENE. LOS DATOS SE 
-- DESCARGARON EN FORMATO 'TXT', EL ARCHIVO TIENE FORMATO 'CSV' SEPARADO
-- POR PIPES ( '|' )
LOAD DATA 
LOCAL INFILE 'sepomex_data.csv'
INTO TABLE tmp_tbl_sepomex_data
FIELDS TERMINATED BY '|'
LINES TERMINATED BY '\n'
IGNORE 2 LINES;

-- SECCION DE CREACION DE TABLAS A UTILIZAR PARA EL PROYECTO

CREATE TABLE cat_tipo_colonia (
    id               INT NOT NULL AUTO_INCREMENT,
    clave            CHAR(2) NOT NULL UNIQUE,
    tipo_colonia     VARCHAR(25) NOT NULL,
    PRIMARY KEY ( id )
);

CREATE TABLE tbl_estados (
    id                INT NOT NULL AUTO_INCREMENT,
    clave_estado      CHAR(2) NOT NULL,
    nombre_estado     VARCHAR(35) NOT NULL,
    PRIMARY KEY ( id )
);

CREATE TABLE tbl_municipio (
    id                           INT NOT NULL AUTO_INCREMENT,
    clave_municipio              CHAR(2) NOT NULL,
    clave_colonia                CHAR(4) NOT NULL,
    clave_oficina                CHAR(5) NOT NULL,
    clave_estado                 CHAR(2) NOT NULL,
    clave_ciudad                 CHAR(2),
    nombre_ciudad                VARCHAR(50),
    nombre                       VARCHAR(50) NOT NULL,
    PRIMARY KEY ( id ) 
);

CREATE TABLE tbl_colonia (
    id                         INT NOT NULL AUTO_INCREMENT,
    codigo_postal              CHAR(5) NOT NULL,
    codigo_oficina             CHAR(5) NOT NULL,
    clave_municipal            CHAR(4) NOT NULL,
    nombre                     VARCHAR(65) NOT NULL,
    tipo_zona                  VARCHAR(10) NOT NULL,
    clv_tipo_col               CHAR(2) NOT NULL,
    clv_municipio              CHAR(2) NOT NULL,
    PRIMARY KEY ( id )
);

-- SECCIÓN DE LLENADO DE LA INFORMACIÓN POR CATALOGO Y TABLAS

INSERT INTO cat_tipo_colonia( clave, tipo_colonia )
SELECT DISTINCT tmp_clv_tcol, tmp_t_col FROM tmp_tbl_sepomex_data ORDER BY 1;

INSERT INTO tbl_estados( clave_estado, nombre_estado )
SELECT DISTINCT tmp_clv_estado, tmp_nom_estado FROM tmp_tbl_sepomex_data ORDER BY 1;

INSERT INTO tbl_municipio( clave_municipio, clave_colonia, clave_oficina, clave_estado, clave_ciudad, nombre_ciudad, nombre )
SELECT DISTINCT tmp_clv_mnpio, tmp_clv_colmnpio, tmp_clv_oficina, tmp_clv_estado, tmp_clv_ciudad, tmp_nom_ciudad, tmp_nom_mnpio
FROM tmp_tbl_sepomex_data
GROUP BY 3, 1
ORDER BY 1, 3, 2;

INSERT INTO tbl_colonia( codigo_postal, codigo_oficina, clave_municipal, nombre, tipo_zona, clv_tipo_col, clv_municipio )
SELECT DISTINCT tmp_cp, tmp_cod_oficina, tmp_clv_colmnpio, tmp_nom_col, tmp_tzona, tmp_clv_tcol, tmp_clv_mnpio
FROM tmp_tbl_sepomex_data
GROUP BY 1, 2
ORDER BY 1, 2, 3;
